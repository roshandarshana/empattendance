<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/edit-user', 'UserController@getEditPage')->name('user-edit');
Route::post('post-edit-user', 'UserController@edit')->name('post-edit-user');

Route::post('submit-attendance', 'AttendanceController@submitAttendance')->name('submit-attendance');
