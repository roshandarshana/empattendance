@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (Session::has('status'))
                        <div class="alert alert-{{Session::get('type')}}" role="alert">
                            {{ Session::get('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('submit-attendance') }}">
                        @csrf
                        <div class="form-group row mb-0">
                            <div class="col-md-8">
                                @error('name')
                                <span class="alert alert-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="col-md-4">
                                <button type="submit" name="attendance" value="1" class="btn btn-success col-5">
                                    {{ __('In') }}
                                </button>
                                <button type="submit" name="attendance" value="0" class="btn btn-danger col-5">
                                    {{ __('Out') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                @if(count($attendances) > 0)
                <div class="card-footer">
                    <h2>Attendance</h2>
                    <table class="table table-striped col-8 offset-2">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Type</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($attendances as $key => $attendance)
                        <tr>
                            <td>{{$attendance->created_at->format('d.m.Y')}}</td>
                            <td>{{$attendance->created_at->format('H:i:s')}}</td>
                            <td>{{$attendance->type == 1 ? 'In' : 'Out'}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="h-100 d-flex justify-content-center">
                        {{ $attendances->links() }}
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
