<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'name' => 'required',
            'user_name' => 'required|unique:users,id,'.$this->get('user_id'),
            'email' => 'required|email|unique:users,id,'.$this->get('user_id'),
            'contact' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'user_id.exists' => 'User does not exists.',
            'user_name.unique' => 'User name must be unique.',
        ];
    }
}
