<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserEditRequest;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Http\Request;
use Session;
use Exception;
use DB;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(UserEditRequest $request)
    {
        try {
            $user = User::find($request->get('user_id'));
            $user->name = $request->get('name');
            $user->user_name = $request->get('user_name');
            $user->email = $request->get('email');
            $user->contact = $request->get('contact');
            DB::transaction(function () use ($user) {
                $user->save();
            });
        } catch (Exception $e) {
            \Log::error($e->getMessage());
            Session::flash('status', 'Something went wrong.');
            Session::flash('type', 'danger');
            return redirect()->back();
        }

        Session::flash('status', 'Successfully updated.');
        Session::flash('type', 'success');
        return redirect()->back();
    }

    public function getEditPage() {
        return view('user_edit')
            ->with('user', Auth::user());
    }
}
