<?php

namespace App\Http\Controllers;

use Session;
use Exception;
use App\Attendance;
use App\Http\Requests\AttendanceSubmitRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AttendanceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function submitAttendance(AttendanceSubmitRequest $request)
    {
        try {
            Attendance::create([
                'user_id' => Auth::id(),
                'type' => $request->get('attendance'),
            ]);

            Session::flash('status', 'Successfully updated.');
            Session::flash('type', 'success');
        } catch (Exception $e) {
            \Log::error($e->getMessage());
            Session::flash('status', 'Something went wrong.');
            Session::flash('type', 'danger');
        }
        return redirect()->back();
    }
}
