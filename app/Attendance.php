<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $fillable = [
        'type',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
